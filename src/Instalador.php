<?php

namespace Ciebit\Composer;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class Instalador extends LibraryInstaller
{
    const MODULOS_TIPOS = [
        'php' => [
            'id' => 'ciebit-cb',
            'prefixo' => 'ciebit/cb-',
            'pasta' => 'cb'
        ],
        'js' => [
            'id' => 'ciebit-js',
            'prefixo' => 'ciebit/js-',
            'pasta' => 'cb/js'
        ]
    ];

    public function getInstallPath(PackageInterface $Pacote)
    {
        $prefixo = substr($Pacote->getPrettyName(), 0, 10);

        foreach (self::MODULOS_TIPOS as $linguagem => $dados)
        {
            if ($dados['prefixo'] == $prefixo) {
                return $dados['pasta'] .'/'. substr($Pacote->getPrettyName(), 10);
            }
        }

        throw new \InvalidArgumentException(
            "O pacote {$Pacote->getName()} pode não pertecer "
            ."ao projeto Ciebit/cb"
        );

    }

    public function supports($id)
    {
        foreach (self::MODULOS_TIPOS as $linguagem => $dados) {
            if ($id == $dados['id']) {
                return true;
            }
        }

        return false;
    }
}
