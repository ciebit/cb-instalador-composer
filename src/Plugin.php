<?php

namespace Ciebit\Composer;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

class Plugin implements PluginInterface
{
    public function activate(Composer $Composer, IOInterface $Io)
    {
        $Instalador = new Instalador($Io, $Composer);
        $Composer->getInstallationManager()->addInstaller($Instalador);
    }
}
